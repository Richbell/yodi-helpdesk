<?php
session_start();
require 'config/constante.php';
if (!isset($_SESSION['userinfo'])) {
  // repartir se connecter
  echo '<script type="text/javascript">
    window.location.href = "login.php";
  </script>';
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link href="img/logo/logo.png" rel="icon">  
  <title>YODI Support - Tableau de bord</title>
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/ruang-admin.min.css" rel="stylesheet">
</head>

<body id="page-top">
  <div id="wrapper">

  <?php require 'sidebar.php'; ?>

    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">

        <?php require 'topbar.php'; ?>

        <!-- Container Fluid-->
        <div class="container-fluid" id="container-wrapper">
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Tableau de bord</h1>
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="./">Accueil</a></li>
              <li class="breadcrumb-item active" aria-current="page">Tableau de bord</li>
            </ol>
          </div>


          <div class="row mb-3">
            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card h-100">
                <div class="card-body">
                  <div class="row align-items-center">
                    <div class="col mr-2">


                   
                      <div class="text-xs font-weight-bold text-uppercase mb-1">Tickets du mois</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800" id = "mois">0</div>
                        <!--<div class="mt-2 mb-0 text-muted text-xs">
                        <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                        <span>Since last month</span>
                      </div> -->
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-calendar fa-2x text-primary"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

           
            <!-- Pending Requests Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card h-100">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-uppercase mb-1">Requêtes en attente</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800" id = "attente">0</div>

                    </div>
                    <div class="col-auto">
                      <i class="fas fa-comments fa-2x text-warning"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            
            <!-- Earnings (Annual) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card h-100">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-uppercase mb-1">Tickets résolus</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800" id = "resolus">0</div>
                      <!--<div class="mt-2 mb-0 text-muted text-xs">
                        <span class="text-success mr-2"><i class="fas fa-arrow-up"></i> 12%</span>
                        <span>Since last years</span>
                      </div>-->
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-check-square fa-2x text-success"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

       

            <!-- New User Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card h-100">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-uppercase mb-1">Tickets rejetés</div>
                      <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800" id = "rejete">0</div>

                    </div>
                    <div class="col-auto">
                      <i class="fas fa-ban fa-2x text-danger"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>




        </div>
        <!---Container Fluid-->
      </div>
      <?php require 'footer.php'; ?>
    </div>
  </div>

  <!-- Scroll to top -->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <script>
//fonction pour le chargement des statistiques en BG

    function actualise_stat()
{
     // 1 : on récupère les données 
     var xhr = new XMLHttpRequest();
      // 1 bis : pour récupérer les données (les donnés sont chargés sur la page charge_statistique.php en bg)
      xhr.open("GET", "charge_statistique.php", true);
      xhr.send(null);

     xhr.onreadystatechange = function()
     {
          if(xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
          {
               // 2 : on découpe les résultats dans un tableau grâce aux '-' que j'affiche après chaque valeur
               var resultats = xhr.responseText.split("-");
               // 3 : on les affiche 
               document.getElementById("mois").innerHTML = resultats[0];
               document.getElementById("attente").innerHTML = resultats[1];
               document.getElementById("resolus").innerHTML = resultats[2];
               document.getElementById("rejete").innerHTML = resultats[3];
         }
     }
    
}
 
// fonction nécessaire pour l'exécution du XMLHttp Request
function getXMLHttpRequest() {
        var xhr = null;
  
        if(window.XMLHttpRequest || window.ActiveXObject) {
                if(window.ActiveXObject) {
                        try {
                                xhr = new ActiveXObject("Msxml2.XMLHTTP");
                        } catch(e) {
                                xhr = new ActiveXObject("Microsoft.XMLHTTP");
                        }
                } else {
                        xhr = new XMLHttpRequest();
                }
        } else {
                alert("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
                return null;
        }
  
        return xhr;
}

//première appel à l'initialisation de la page
actualise_stat();

  </script>

  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="js/ruang-admin.min.js"></script>
  <script src="vendor/chart.js/Chart.min.js"></script>
  <script src="js/demo/chart-area-demo.js"></script>
</body>

</html>
