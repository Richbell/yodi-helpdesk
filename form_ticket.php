<?php
  session_start();
  require 'config/constante.php';
  if (!isset($_SESSION['userinfo'])) {
    // repartir se connecter
    echo '<script type="text/javascript">
      window.location.href = "login.php";
    </script>';
  }
  $_SESSION['nomfichier'] = null;


  //Liste des types de tickets
  $url = 'http://yodiws.yodingenierie.com/wsyodisupport/listetypeticket';
  $curlinit0 = curl_init($url);
  curl_setopt($curlinit0, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
  curl_setopt($curlinit0, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curlinit0, CURLOPT_USERPWD, "$ws_username:$ws_password");
  curl_setopt($curlinit0, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  $curl_response_liste = curl_exec($curlinit0);
  if ($curl_response_liste === false) {
  $infoinit0 = curl_getinfo($curlinit0);
  curl_close($curlinit0);
  die('error occured during curl exec. Additioanl info: ' . var_export($infoinit0));
  } else {
    //parcourir le résultat et afficher
    $liste_typeticket = json_decode($curl_response_liste,true);
  }

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link href="img/logo/logo.png" rel="icon">
  <title>YODI Support - Créer un ticket</title>


  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/ruang-admin.min.css" rel="stylesheet">



  <link href="bootstrap-fileinput-master/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
  <link href="bootstrap-fileinput-master/themes/explorer-fas/theme.css" media="all" rel="stylesheet" type="text/css"/>
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>

  <script src="bootstrap-fileinput-master/js/plugins/piexif.js" type="text/javascript"></script>
  <script src="bootstrap-fileinput-master/js/plugins/sortable.js" type="text/javascript"></script>
  <script src="bootstrap-fileinput-master/js/fileinput.js" type="text/javascript"></script>
  <script src="bootstrap-fileinput-master/js/locales/fr.js" type="text/javascript"></script>
  <script src="bootstrap-fileinput-master/themes/fas/theme.js" type="text/javascript"></script>
  <script src="bootstrap-fileinput-master/themes/explorer-fas/theme.js" type="text/javascript"></script>


</head>

<body id="page-top">
  <div id="wrapper">
    <?php require 'sidebar.php'; ?>

    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
              <?php require 'topbar.php'; ?>

        <!-- Container Fluid-->
        <div class="container-fluid" id="container-wrapper">
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Enregistrer un ticket</h1>
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="./">Accueil</a></li>
              <li class="breadcrumb-item active" aria-current="page">Enregistrer un ticket</li>
            </ol>
          </div>

          <div class="row justify-content-center">
            <div class="col-lg-8">
            <!-- Formulaire création de ticket-->
            <div class="card mb-4">
              <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Veuillez remplir ce formulaire</h6>
              </div>
              <div class="card-body fticket">
                <form method="post" id="formticket" action="saveticket.php">
                  <div class="form-group">
                    <label for="exampleFormControlReadonly">Expéditeur de la demande</label>
                    <input class="form-control" type="text" placeholder="" name="suser" id="exampleFormControlReadonly" value="<?php echo $_SESSION['userinfo']['mail_pro_user']; ?>" readonly>
                      <input type="hidden" placeholder="" name="sidticket" id="" readonly>
                      <input type="hidden" placeholder="" name="sent" value="0" id="" readonly>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Sélectionner le type de la demande</label>
                    <select class="form-control ctypedemande" id="exampleFormControlSelect1" name="stypedemande">
                      <?php
                          foreach ($liste_typeticket as $untype) {
                      ?>
                      <option value="<?php echo $untype['IDType_Travaux'] ?>"> <?php echo $untype['Designation_type_travaux']?></option>

                    <?php } ?>
                    </select>
                  </div>

                  <div class="form-group">
                    <label>Quelle est la priorité de votre demande ?</label>
                    <div class="custom-control custom-radio">
                      <input value="Basse" checked type="radio" id="customRadio3" name="spriorite" class="custom-control-input">
                      <label class="custom-control-label" for="customRadio3">Basse</label>
                    </div>
                    <div class="custom-control custom-radio">
                      <input  value="Moyenne"  type="radio" id="customRadio4" name="spriorite" class="custom-control-input">
                      <label class="custom-control-label" for="customRadio4">Moyenne</label>
                    </div>
                    <div class="custom-control custom-radio">
                      <input  value="Haute"  type="radio" name="spriorite" id="customRadio5" class="custom-control-input">
                      <label class="custom-control-label" for="customRadio5">Haute</label>
                    </div>
                  </div>

                          <!---Gallerie de capture-->
                        <div class="form-group uploader">
                          <label>Ajouter vos pièces jointes (Capture d'écrans etc...)</label>
                          <div class="file-loading">
                              <input id="file-fr" name="file-fr[]" type="file"  multiple>
                          </div>
                        </div>

                        
                          <!---fin gallerie de capture-->
                        <hr>
                  <button type="submit" class="btn btn-primary float-right">Enregistrer la demande</button>
                </form>
              </div>
            </div>

          </div>

          </div>

        </div>


        <!---Container Fluid-->
      </div>
      <?php require 'footer.php'; ?>
    </div>
  </div>

  <!-- Scroll to top -->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>


  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="js/ruang-admin.min.js"></script>

</body>

<script>
    (function( $ ){
     $.fn.envoiedefichier = function(_callback) {

       //Envoyer les fichiers le serveur si existe
       var filesCount = $('#file-fr').fileinput('getFilesCount'); // returns count of files (pending upload)
       if (filesCount > 0) {
           $('#file-fr').fileinput('upload');
       }else {
              _callback();
       }
       console.log('Nombre de fichier : ',filesCount);
     };
    })( jQuery );

    (function( $ ){
     $.fn.saveticket = function() {
       //Envoyer les fichiers le serveur si existe
       $('.description').removeClass('has-error'); // remove the error class
       $('.invalid-feedback').remove(); // remove the error text

         // get the form data
         // there are many ways to get this data using jQuery (you can use the class or id also)
         	console.log($('input[name=spriorite]:checked','.fticket').val());
         var formData = {
             'sidticket'              : $('input[name=sidticket]').val(),
             'suser'              : $('input[name=suser]').val(),
             'stypedemande'             : $('select.ctypedemande').children("option:selected").val(),
             'spriorite'    : $('input[name=spriorite]:checked','.fticket').val(),
             'sdescription'    : $('#champ-description').val()
         };
         // process the form
         $.ajax({
             type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
             url         : 'saveticket.php', // the url where we want to POST
             data        : formData, // our data object
             dataType    : 'json', // what type of data do we expect back from the server
             encode      : true,
         }).done(function(data) {  // using the done promise callback
                 // log data to the console so we can see
                 console.log(data);
               // here we will handle errors and validation messages
               if ( ! data.success) {
                   // handle errors for name ---------------
                   if (data.errors.sdescription) {
                       $('#champ-description').addClass('is-invalid');
                       $('#description-group').append('<div class="invalid-feedback">' + data.errors.sdescription + '</div>');
                   }
               } else {
                 // ALL GOOD! just show the success message!
                 $('#formticket').html('<div class="alert alert-' + data.message_type + '" role="alert">'+
                 '<h6><i class="fas fa-check"> &nbsp;</i><b> Fin du traitement !</b>'+
                 '</h6>' + data.message + '</div>'+
                 '<div align="center"> <a href="mestickets.php" class="btn btn-light btn-icon-split">'+
                       '<span class="icon text-white-50">'+
                           '<i class="fas fa-tasks"></i>'+
                         '</span>'+
                         '<span class="text">Afficher la liste des tickets</span>'+
                       '</a></div>'
               );
               }
             }).fail(function(data) {
               //Server failed to respond - Show an error message
               $('#formticket').html('<div class="alert alert-danger">Could not reach server, please try again later.</div>');
             });

             console.log('Enregistrement dans la base termine');
     };

    })( jQuery );

    $('#file-fr').fileinput({
      uploadExtraData: {

        },
        fileActionSettings: {
            showUpload: false,
            showDrag: false,
        },
        showUpload: false,
        showCaption : false,
        showClose : false,
        theme: 'fas',
        language: 'fr',
        uploadUrl: 'uploadfiles.php',
        uploadAsync :false,
        overwriteInitial: false,
        allowedFileExtensions: ['jpg', 'png','jpeg','pdf'],

        maxFileSize: 24000,
        maxFilesNum: 10,

        initialPreview: [
          "http://lorempixel.com/800/460/people/1",
          "http://lorempixel.com/800/460/people/2"
        ],
        initialPreviewAsData: true,
        initialPreviewConfig: [
           { url: "", key: 1, showRemove : false,},
           { url: "", key: 2, showRemove : false,},
       ],

    }).on('fileuploaded', function(event, previewId, index, fileId) {
        console.log('File Uploaded', 'ID: ' + fileId + ', Thumb ID: ' + previewId);
    }).on('fileuploaderror', function(event, data, msg) {
        console.log('File Upload Error', 'ID: ' + data.fileId + ', Thumb ID: ' + data.previewId);
    }).on('filebatchuploadcomplete', function(event, preview, config, tags, extraData) {

        console.log('File Batch Uploaded', preview, config, tags, extraData);
    }).on('filebatchuploadsuccess', function(event, data) {
       console.log('File Batch success', data);
        $(document).saveticket();
    });


    $(document).ready(function () {
        // A la validation du formulaire

        $('#formticket').submit(function(event) {

              event.preventDefault();
              $('input[name=sent]').val('0');
              $(document).envoiedefichier(function() {
              $(document).saveticket();
                  });

        });



    });
</script>

</html>
