<?php
  session_start();
  require 'config/constante.php';
  if (!isset($_SESSION['userinfo'])) {
    // repartir se connecter
    echo '<script type="text/javascript">
      window.location.href = "login.php";
    </script>';
  }
  $_SESSION['nomfichier'] = null;


  //Liste des types de tickets
  $url = 'http://yodiws.yodingenierie.com/wsyodisupport/listeticket/'.$_SESSION['userinfo']['mail_pro_user'].'/0';
  $curlinit0 = curl_init($url);
  curl_setopt($curlinit0, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
  curl_setopt($curlinit0, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curlinit0, CURLOPT_USERPWD, "$ws_username:$ws_password");
  curl_setopt($curlinit0, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  $curl_response_liste = curl_exec($curlinit0);
  if ($curl_response_liste === false) {
  $infoinit0 = curl_getinfo($curlinit0);
  curl_close($curlinit0);
  die('error occured during curl exec. Additioanl info: ' . var_export($infoinit0));
  } else {
    //parcourir le résultat et afficher
    $liste_ticket = json_decode($curl_response_liste,true);
  }

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link href="img/logo/logo.png" rel="icon">
  <title>YODI Support -Mes tickets</title>
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/ruang-admin.min.css" rel="stylesheet">
</head>

<body id="page-top">
  <div id="wrapper">
    <?php require 'sidebar.php'; ?>

    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
              <?php require 'topbar.php'; ?>

        <!-- Container Fluid-->
        <div class="container-fluid" id="container-wrapper">
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Mes tickets</h1>
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="./">Accueil</a></li>
              <li class="breadcrumb-item active" aria-current="page">Mes tickets</li>
            </ol>
          </div>


          <div class="row">
        <div class="col-lg-12 mb-4">
          <!-- Simple Tables -->
          <div class="card">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
              <h6 class="m-0 font-weight-bold text-primary">Liste des demandes d'intervention</h6>
            </div>
            <div class="table-responsive">
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th>N° Ticket</th>
                    <th>Date envoie</th>
                    <th>Expéditeur</th>
                    <th>Description</th>
                    <th>Statut</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>

                  <?php
                      foreach ($liste_ticket as $unticket) {
                  ?>
                  <tr>
                    <td><?php echo $unticket['IDTickets'] ?></td>
                    <td><?php echo $unticket['date_demande_hl'] ?></td>
                    <td><?php echo $unticket['Nom_Prenom_User'] ?></td>
                    <td><?php echo $unticket['description_demande_min'] ?></td>
                    <td><span class="badge badge-<?php echo $unticket['statut_badge'] ?>"><?php echo $unticket['statut_lib'] ?></span></td>

                    <td>
                      <form method="post" action="save_ticket">
                           <button type="submit" class="btn btn-sm btn-primary">Modifier</button>
                           <button type="submit" class="btn btn-sm btn-primary">Supprimer</button>
                      </form>

                    </td>
                  </tr>
                  <?php
                }
                  ?>

                </tbody>
              </table>
            </div>
            <div class="card-footer"></div>
          </div>
        </div>
      </div>

        </div>


        <!---Container Fluid-->
      </div>
      <?php require 'footer.php'; ?>
    </div>
  </div>

  <!-- Scroll to top -->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="js/ruang-admin.min.js"></script>

</body>

</html>
