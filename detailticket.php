<?php
  session_start();
  require 'config/constante.php';
  if (!isset($_SESSION['userinfo'])) {
    // repartir se connecter
    echo '<script type="text/javascript">
      window.location.href = "login.php";
    </script>';
  }

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link href="img/logo/logo.png" rel="icon">
  <title>YODI Support - Détail ticket</title>

  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/ruang-admin.min.css" rel="stylesheet">


  <link href="bootstrap-fileinput-master/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
  <link href="bootstrap-fileinput-master/themes/explorer-fas/theme.css" media="all" rel="stylesheet" type="text/css"/>
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>

  <script src="bootstrap-fileinput-master/js/plugins/piexif.js" type="text/javascript"></script>
  <script src="bootstrap-fileinput-master/js/plugins/sortable.js" type="text/javascript"></script>
  <script src="bootstrap-fileinput-master/js/fileinput.js" type="text/javascript"></script>
  <script src="bootstrap-fileinput-master/js/locales/fr.js" type="text/javascript"></script>
  <script src="bootstrap-fileinput-master/themes/fas/theme.js" type="text/javascript"></script>
  <script src="bootstrap-fileinput-master/themes/explorer-fas/theme.js" type="text/javascript"></script>


  <style>
    .detail-line{
      padding-bottom: 10px;
      border-bottom: dashed  2px #ccc;
    }
    .notification {
      background: #f0f0f0 ;
      border-radius:50px;

    }

    .bubble1
  {
  position: relative;
  right: 0px;
  margin-top: 0px;
  padding: 0px;
  background: #3abaf4;
  -webkit-border-radius: 5px;
  -moz-border-radius: 5px;
  border-radius: 5px;
  max-width: 85%;
  min-width: 60px;
  }

  .bubble1:after
  {
  content: '';
  position: absolute;
  border-style: solid;
  border-width: 0 18px 20px;
  border-color: #3abaf4 transparent;
  display: block;
  width: 0;
  z-index: 1;
  top: -10px;
  right: 0;
  margin-right: 15px
}
.bubble2
  {
  position: relative;
  margin-top: 0px;
  padding: 0px;
  background: #e0e0e0;
  -webkit-border-radius: 5px;
  -moz-border-radius: 5px;
  border-radius: 5px;
  max-width: 85%;
    min-width: 60px;

  }

  .bubble2:after
  {
  content: '';
  position: absolute;
  border-style: solid;
  border-width: 0 18px 20px;
  border-color: #e0e0e0 transparent;
  display: block;
  width: 0;
  z-index: 1;
  top: -10px;
  left: 0;
  margin-left: 15px
}

.wall {
        display: block;
        position: relative;
        padding-top: 0px;

        }

      .wall-column {
        display: block;
        position: relative;
        width: 33%;
        float: left;
        padding: 0 5px;
        box-sizing: border-box;

      }


      .wall-item {
       margin-top: 10px;
        transition: all 220ms;

      }

      .wall-item img{

      margin: 0 !important;
      border-radius: 5px;
      }

      .wall-item img:hover{
        transform: scale(1.01) !important;
      }
  </style>
</head>

<body id="page-top">
  <div id="wrapper">
    <?php require 'sidebar.php';
        //Détail d'un ticket
        $url = 'http://yodiws.yodingenierie.com/wsyodisupport/detail_ticket/'.$_GET['idticket'];
        $curlinit0 = curl_init($url);
        curl_setopt($curlinit0, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($curlinit0, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlinit0, CURLOPT_USERPWD, "$ws_username:$ws_password");
        curl_setopt($curlinit0, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $curl_response_detail = curl_exec($curlinit0);
        if ($curl_response_detail === false) {
        $infoinit0 = curl_getinfo($curlinit0);
        curl_close($curlinit0);
        die('error occured during curl exec. Additioanl info: ' . var_export($infoinit0));
        } else {
          //parcourir le résultat et afficher
          $detail_ticket = json_decode($curl_response_detail,true);
        }
    
    ?>



    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
              <?php require 'topbar.php'; ?>

        <!-- Container Fluid-->
        <div class="container-fluid" id="container-wrapper">
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Détails ticket</h1>
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="./">Accueil</a></li>
              <li class="breadcrumb-item active" aria-current="page">Détails ticket</li>
            </ol>
          </div>

<?php
  if (!isset($_GET['idticket'])) {
    ?>
    <div class="text-center">
      <img src="img/error.svg" style="max-height: 100px;" class="mb-3">
      <h3 class="text-gray-800 font-weight-bold">Oopss!</h3>
      <p class="lead text-gray-800 mx-auto">404 Vous êtes perdu ?</p>
      <a href="index.php">&larr; Revenir à l'accueil</a>
    </div>
    <?php
  }else {
  ?>

  <div class="row">
<div class="col-lg-4">
  <div class="card shadow-sm mb-4">
    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between"  style="background:#e0e0e0">
      <h6 class="m-0 font-weight-bold text-primary"><span style="color:#2e2e2e">Détails du ticket</span></h6>
    </div>
    <div class="card-body">
      <h6 class="detail-line" style=""> N° Ticket : <span class="" style="font-weight:bolder"> <?php echo $detail_ticket['IDTickets'] ?> </span></h6>
      <h6 class="detail-line" style=""> Type de demande : <span style="font-weight:bolder"><?php echo $detail_ticket['Designation_type_travaux'] ?> </span></h6>
      <h6 class="detail-line" style=""> Date d'envoie : <span class="float-right" style="font-weight:bolder"> <?php echo $detail_ticket['date_demande_hl'] ?>  </span></h6>
      <h6 class="detail-line" style=""> Priorité du ticket : <span class="float-right" style="font-weight:bolder"> <?php echo $detail_ticket['priorite_ticket'] ?>  </span></h6>
      <h6 class="detail-line" style=""> Statut de la demande : <span class="float-right badge badge-<?php echo $detail_ticket['statut_badge'] ?>" > <?php echo $detail_ticket['statut_lib'] ?> </span></h6>
      <h6 class="detail-line" style=""> Date liv. prévu : <span class="float-right"> <?php echo $detail_ticket['date_livraison_hl'] ?> </span></h6>
    </div>
  </div>

  <div class="card shadow-sm mb-4">
    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between"  style="background:#e0e0e0">
      <h6 class="m-0 font-weight-bold text-primary"><span style="color:#2e2e2e">Progression</span></h6>
    </div>
    <div class="card-body">
              <div class="progress">
                <div class="progress-bar progress-bar-striped" role="progressbar" style="width: <?php echo $detail_ticket['Niveau_de_traitement']."%" ?>;" aria-valuenow = "50"
                  aria-valuemin="0" aria-valuemax="100"><?php echo $detail_ticket['Niveau_de_traitement']. " %" ?> </div>
              </div>
    </div>
  </div>

        <div class="card shadow-sm mb-4">
              <div class="card-header py-3" style="background:#e0e0e0">
                <h6 class="m-0 font-weight-bold text-primary"><span style="color:#2e2e2e">Actions</span></h6>
              </div>
              <div class="card-body">

              <form method="post" action="form_ticket.php">
                     <input type="hidden" value="<?php echo $unticket['IDTickets'] ?>" name="idticket" />
                     <button type="submit" class="btn btn-primary mb-1" <?php if ($detail_ticket['statut_lib'] <> "En attente") echo "disabled"; ?>><i class="fas fa-edit"></i>&nbsp;Modifier</button>
                     <button type="button" class="btn btn-danger mb-1" <?php if ($detail_ticket['statut_lib'] <> "En attente") echo "disabled"; ?>><i class="fas fa-trash"></i>&nbsp;Supprimer</button>
              </form>


                 
                  

              </div>
            </div>
</div>




<div class="col-lg-8">
  <div class="card shadow-sm mb-4">
    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between"  style="background:#e0e0e0">
      <h6 class="m-0 font-weight-bold text-primary"><span style="color:#2e2e2e">Commentaires</span></h6>
    </div>
    <div class="card-body" style="position: relative;">
      <div class="row">
  <div class="col-lg-12 col-xs-12 row chat-text" style="position:relative; min-height: 360px; max-height:500px; overflow:auto">

    <?php
        foreach ($detail_ticket['tab_commentaire'] as $uncommentaire) {
          if ($uncommentaire['mail_pro_user'] == $_SESSION['userinfo']['mail_pro_user']) {
            ?>

            <!-- bulle 1 -->
            <div class="col-lg-12 col-xs-12 float-right" style="margin-top: 20px">
            <div class="col-lg-12 col-xs-12" align="right">
            <h6><font color="#ccc"> <?php echo $uncommentaire['date_commentaire_hl'] ?> </font>&nbsp;&nbsp;&nbsp;
              <font color="#222"><strong><?php echo $uncommentaire['Nom_Prenom_User'] ?></strong></font>&nbsp;<i class="fa fa-circle" style="font-size: 7px; color: #00b0f0;"></i></h6>
            </div>
            <div class="bubble1 float-right" >
            <p  style="color:#FFF; padding:10px; font-size:14px">
              <?php echo $uncommentaire['Commentaire_texte'] ?>
            </p>
            </div>
            </div>

            <?php
          } else {
                  
                if (empty($uncommentaire['mail_pro_user'])) {
                  ?>

                  <!-- notification  -->
                  <div style="margin-top: 20px; width :60%; margin-left: auto; margin-right: auto" >
                    <div class="notification" >
                      <p  style="color:#333; padding:10px; font-size:14px; font-weight :bold; text-align:center">
                        <?php echo $uncommentaire['Commentaire_texte']. " " . $uncommentaire['date_commentaire_hl'] ?>
                      </p>
                    </div>
                  </div>

                  <?php
                } else {
                  ?>

            <!-- bulle 2 -->
            <div class="col-lg-12 col-xs-12" style="margin-top: 20px">
              <div class="col-lg-12 col-xs-12">
                <h6><font color="#222"><strong><?php echo $uncommentaire['Nom_Prenom_User'] ?></strong></font>&nbsp;&nbsp;&nbsp;
                  <font color="#ccc">  <?php echo $uncommentaire['date_commentaire_hl'] ?></font></h6>
              </div>
              <div class="bubble2 float-left">
                <p  style="color:#111; padding:10px; font-size:14px">
                  <?php echo $uncommentaire['Commentaire_texte'] ?>
                </p>
              </div>
            </div>

            <?php
              }
          }
          ?>

    <?php
        }
    ?>
    <br>
    &nbsp;
</div>


      <div class="col-lg-12 col-xs-12 chat-message" >
        <form method="post">
          <div class="form-group">
              <textarea required class="form-control" name="commentaire" id="commentaire" placeholder ="Votre message ici" rows="3"></textarea>
          </div>
              <div class="form-group">
                       <button type="submit" class="btn btn-primary">Envoyer</button>
              </div>

       </form>
       </div>

</div>



    </div>
  </div>
</div>

<div class="col-lg-12">
  <div class="card shadow-sm mb-4">
    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between"  style="background:#e0e0e0">
      <h6 class="m-0 font-weight-bold text-primary"><span style="color:#2e2e2e">Pièces jointes</span></h6>
    </div>
    <div class="card-body">

      <div class="file-loading">
          <input id="file-fr" name="file-fr[]" type="file"  multiple>
      </div>

    </div>
  </div>

</div>

</div>

  <?php
  }
?>





        </div>


        <!---Container Fluid-->
      </div>
      <?php require 'footer.php'; ?>
    </div>
  </div>

  <!-- Scroll to top -->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>



  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="js/ruang-admin.min.js"></script>

</body>

<script>


$('#file-fr').fileinput({

    fileActionSettings: {
        showUpload: false,
        showDrag: false,
        showRemove : false,
    },
    initialPreview: [
      <?php
          foreach ($detail_ticket['tab_piece_jointe'] as $unpj) {
              echo '"'.$unpj['urlpj'].'",';
          }
        ?>

    ],
    initialPreviewAsData: true,
    initialPreviewConfig: [

      <?php
          foreach ($detail_ticket['tab_piece_jointe'] as $unpj) {
              echo '{ url: "", key: '.$unpj['IDPiece_jointe'].', showRemove : false,},';
          }
        ?>

   ],
    showUpload: false,
    showCaption : false,
    showClose : false,
    showBrowse : false,
    showRemove : false,
    dropZoneEnabled : false,
    dropZoneTitle : 'Aucun média',
    theme: 'fas',
    language: 'fr',
    uploadAsync :false,

}).on('fileuploaded', function(event, previewId, index, fileId) {
    console.log('File Uploaded', 'ID: ' + fileId + ', Thumb ID: ' + previewId);
}).on('fileuploaderror', function(event, data, msg) {
    console.log('File Upload Error', 'ID: ' + data.fileId + ', Thumb ID: ' + data.previewId);
}).on('filebatchuploadcomplete', function(event, preview, config, tags, extraData) {

    console.log('File Batch Uploaded', preview, config, tags, extraData);
}).on('filebatchuploadsuccess', function(event, data) {
   console.log('File Batch success', data);
    $(document).saveticket();
});


</script>

</html>
