<?php
session_start();
$ws_username = "yodidev";
$ws_password = "dev@Yodi20";

$errors         = array();      // array to hold validation errors
$data           = array();      // array to pass back data

// validate the variables ======================================================
// if any of these variables don't exist, add an error to our $errors array
if (empty($_POST['sdescription']))
    $errors['sdescription'] = 'Veuillez décrire votre demande s\'il vous plais !';

// if there are any errors in our errors array, return a success boolean of false
if ( ! empty($errors)) {
    // if there are items in our errors array, return those errors
    $data['success'] = false;
    $data['errors']  = $errors;
} else {

     // Code d'enregistrement
     $unticket = array(
            "IDTickets"  => $_POST['sidticket'],
            "IDType_Travaux" => $_POST['stypedemande'],
            "description_demande" => $_POST['sdescription'],
            "mail_pro_user" => $_POST['suser'],
            "priorite_ticket" => $_POST['spriorite'],
            "listePJ" => implode(',',$_SESSION['nomfichier']),
        );

        //Encodage des input
        $varinput =  json_encode($unticket);
        //Enregistrer le Bia via le webservice

        $service_urlinit0 = "http://yodiws.yodingenierie.com/wsyodisupport/saveticket";
        $curlinit0 = curl_init($service_urlinit0);
        // Set the content type to application/json
        curl_setopt($curlinit0, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        // Attach encoded JSON string to the POST fields
        curl_setopt($curlinit0, CURLOPT_POSTFIELDS, $varinput);
        curl_setopt($curlinit0, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlinit0, CURLOPT_USERPWD, "$ws_username:$ws_password");
        curl_setopt($curlinit0, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $curl_response = curl_exec($curlinit0);
        if ($curl_response === false) {
            $infoinit0 = curl_getinfo($curlinit0);
            curl_close($curlinit0);
            die('error occured during curl exec. Additioanl info: ' . var_export($infoinit0));
        }else {
        //Récupérer le resultat retournée
        $data_reponse = json_decode($curl_response,true);
        //Si tout se passe bien le ticket reviens avec un numero qui est différent de 0
        $_SESSION['IDTickets'] = $data_reponse['IDTickets'];
        if ($data_reponse['IDTickets'] <> 0) {
          // show a message of success and provide a true success variable
          $data['success'] = true;
          $data['message_type'] = 'success';
          $data['message'] = "Votre ticket N° ".$data_reponse['IDTickets']." a été enregistré avec succès !";
        }else {
            $data['success'] = true;
            $data['message_type'] = 'danger';
            $data['message'] = "L'enregistrement du ticket a échoué !";
        }
        }


        curl_close($curlinit0);

}

// return all our data to an AJAX call
echo json_encode($data);

?>
