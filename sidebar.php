<!-- Sidebar -->
<ul class="navbar-nav sidebar sidebar-light accordion" id="accordionSidebar">
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="accueil.php">
    <div class="sidebar-brand-icon">
      <img src="img/logo/logo2.png">
    </div>
    <div class="sidebar-brand-text mx-3">YODI Support</div>
  </a>
  <hr class="sidebar-divider my-0">
  <li class="nav-item active">
    <a class="nav-link" href="index.php">
      <i class="fas fa-fw fa-tachometer-alt"></i>
      <span>Tableau de bord</span></a>
  </li>
  <hr class="sidebar-divider">
  <div class="sidebar-heading">
    Fonctionnalités
  </div>



  <li class="nav-item">
    <a class="nav-link" href="form_ticket.php">
      <i class="fas fa-fw fa-plus-circle"></i>
      <span>Nouveau ticket</span>
    </a>
  </li>

  <li class="nav-item">
    <a class="nav-link" href="mestickets.php">
      <i class="fas fa-fw fa-tasks"></i>
      <span>Mes tickets</span>
    </a>
  </li>

  <li class="nav-item">
    <a class="nav-link" href="#">
      <i class="fas fa-fw fa-bell"></i>
      <span>Mes Notifications</span>
    </a>
  </li>

  <hr class="sidebar-divider">
  <div class="sidebar-heading">
    Paramètres
  </div>

  <li class="nav-item">
    <a class="nav-link" href="#">
      <i class="fas fa-fw fa-users"></i>
      <span>Gestion des utilisateurs</span>
    </a>
  </li>
  <hr class="sidebar-divider">


</ul>
<!-- Sidebar -->
