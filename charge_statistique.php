<?php

session_start();
require 'config/constante.php';
if (!isset($_SESSION['userinfo'])) {
  // repartir se connecter
  echo '<script type="text/javascript">
    window.location.href = "login.php";
  </script>';
}

//j'ai créé une fonction pour le chargement du WS qui en prends en paramètres quelques valeurs 

function chargement_ws($mail,$user,$mdp,$link){
  $url = $link.$mail;
  $curlinit0 = curl_init($url);
  curl_setopt($curlinit0, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
  curl_setopt($curlinit0, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curlinit0, CURLOPT_USERPWD, "$user:$mdp");
  curl_setopt($curlinit0, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  $curl_response = curl_exec($curlinit0);
  if ($curl_response === false) {
  $infoinit0 = curl_getinfo($curlinit0);
  curl_close($curlinit0);
  die('error occured during curl exec. Additioanl info: ' . var_export($infoinit0));
  } else {
  //parcourir le résultat et afficher
  $resultat = json_decode($curl_response,true);
  return $resultat;
  }
}


$reponse_mois = chargement_ws($_SESSION['userinfo']['mail_pro_user'],$ws_username,$ws_password,'http://yodiws.yodingenierie.com/wsyodisupport/nbticket_mois/');
$reponse_attente = chargement_ws($_SESSION['userinfo']['mail_pro_user'],$ws_username,$ws_password,'http://yodiws.yodingenierie.com/wsyodisupport/nbticket_en_attente/');
$reponse_resolu = chargement_ws($_SESSION['userinfo']['mail_pro_user'],$ws_username,$ws_password,'http://yodiws.yodingenierie.com/wsyodisupport/nbticket_resolu/');
$reponse_rejete = chargement_ws($_SESSION['userinfo']['mail_pro_user'],$ws_username,$ws_password,'http://yodiws.yodingenierie.com/wsyodisupport/nbticket_rejete/');

echo $reponse_mois;
echo"-";
echo $reponse_attente;
echo"-";
echo $reponse_resolu;
echo"-";
echo $reponse_rejete;

?>
