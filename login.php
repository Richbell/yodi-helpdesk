<?php
session_start();
require 'config/constante.php';
if (isset($_POST['semail']) && isset($_POST['spassword'])) {
    //Variables d'entrée
  $login = $_POST['semail'];
  $mdp = $_POST['spassword'];

  //lancer la fonction de connection via le webserice
  $url = 'http://yodiws.yodingenierie.com/wsyodisupport/loginuser/'.$login.'/'.$mdp.'';
  $curlinit0 = curl_init($url);
  // Set the content type to application/json
  curl_setopt($curlinit0, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
  curl_setopt($curlinit0, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curlinit0, CURLOPT_USERPWD, "$ws_username:$ws_password");
  curl_setopt($curlinit0, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  $curl_responseinit0 = curl_exec($curlinit0);
  if ($curl_responseinit0 === false) {
  $infoinit0 = curl_getinfo($curlinit0);
  curl_close($curlinit0);
  die('error occured during curl exec. Additioanl info: ' . var_export($infoinit0));
  }else {
    //Récupérer le resultat de la fonction
    $res_login = $curl_responseinit0;
    switch ($res_login) {
      case 1 :
        //Super le code et le mot de passe sont bon
        //on va donc récupérer les informations de l'utilisateur
        $url = 'http://yodiws.yodingenierie.com/wsyodisupport/getinfouser/'.$login;
        $curlinit0 = curl_init($url);
        // Set the content type to application/json
        curl_setopt($curlinit0, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($curlinit0, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlinit0, CURLOPT_USERPWD, "$ws_username:$ws_password");
        curl_setopt($curlinit0, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $curl_response_user = curl_exec($curlinit0);
        if ($curl_response_user === false) {
        $infoinit0 = curl_getinfo($curlinit0);
        curl_close($curlinit0);
        die('error occured during curl exec. Additioanl info: ' . var_export($infoinit0));
        } else {
          //Il faut mettre le data user dans une session
           $data_user = json_decode($curl_response_user,true);
            $_SESSION['userinfo'] = $data_user;
        }
        if($data_user['Mdp_lancement']==1){
          echo '<script type="text/javascript">
            window.location.href = "changepwd.php";
          </script>';
        }else {
          echo '  <script type="text/javascript">
              window.location.href = "index.php";
            </script>';
        }

        break;
    }
  }
  curl_close($curlinit0);
  }

?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link href="img/logo/logo.png" rel="icon">
  <title>YODI Support - Login</title>
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/ruang-admin.min.css" rel="stylesheet">

</head>

<body class="bg-gradient-login">
  <!-- Login Content -->
  <div class="container-login">
    <div class="row justify-content-center">
      <div class="col-xl-6 col-lg-12 col-md-9">
        <div class="card shadow-sm my-5">
          <div class="card-body p-0">
            <div class="row">
              <div class="col-lg-12">
                <div class="login-form">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Connectez vous !</h1>
                  </div>

                  <?php
                    if (isset($_POST['semail']) && isset($_POST['spassword'])) {
                      switch ($res_login) {
                        case 404 :
                        //Compte non trouvé
                        ?>

                        <div class="alert alert-warning alert-dismissible" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          <h6><i class="fas fa-exclamation-triangle"></i><b> Avertissement !</b></h6>
                          Le profil utilisateur référencé n'existe pas.
                        </div>

                        <?php
                        break;
                        case -1 :
                        //Mot de passe incorrect
                        ?>
                  

                          <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                            <h6><i class="fas fa-ban"></i><b> Stop !</b></h6>
                           Le mot de passe est incorrect.
                          </div>

                        <?php
                        break;


                      }
                    }
                  ?>

                  <form class="user" method="post" action="login.php">
                    <div class="form-group">
                      <input type="email" name="semail" class="form-control" id="exampleInputEmail" aria-describedby="emailHelp"
                        placeholder="Entrez votre adresse mail">
                    </div>
                    <div class="form-group">
                      <input type="password" name="spassword" class="form-control" id="exampleInputPassword" placeholder="Mot de passe">
                    </div>

                    <div class="form-group">
                      <button type="submit" class="btn btn-primary btn-block">Connexion </button>
                    </div>

                  </form>


                  <div class="text-center">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Login Content -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="js/ruang-admin.min.js"></script>
</body>

</html>
